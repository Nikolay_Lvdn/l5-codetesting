﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class Query1View
    {
        public GettingProjectView Project { get; set; }
        public int Count { get; set; }
        public override string ToString()
        {
            return $"Project Id: {Project.Id} \nName: {Project.Name} \nDescription: {Project.Description} \nCount: {Count} \n";
        }
    }
}
