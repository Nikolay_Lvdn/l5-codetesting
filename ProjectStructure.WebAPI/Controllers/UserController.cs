﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Business.Interfaces;
using AutoMapper;
using ProjectStructure.Business.DTO;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        // GET: api/<UserController>
        [HttpGet]
        public ActionResult<IList<GettingUserView>> Get()
        {
            var membersDTO = _userService.GetAll();
            var membersView = _mapper.Map<IList<UserDTO>, IList<GettingUserView>>(membersDTO);

            return Ok(membersView);
        }

        // POST api/<UserController>
        [HttpPost]
        public ActionResult Post([FromBody] UserView userView)
        {
            try
            {
                var userDTO = _mapper.Map<UserView, UserDTO>(userView);

                _userService.Create(userDTO);

                return CreatedAtAction("POST", userView);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id,[FromBody] UserView userView)
        {
            try
            {
                var userDTO = _mapper.Map<UserView, UserDTO>(userView);
                userDTO.Id = id;
                _userService.Update(userDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<UserController>/q5
        [HttpGet("q5")]
        public ActionResult<IList<Query5View>> GetOrderedUsersAndTasks()
        {
            try
            {
                var result = _mapper.Map<IList<Query5DTO>, IList<Query5View>>(_userService.GetOrderedUsersAndTasks());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<UserController>/q6
        [HttpGet("q6")]
        public ActionResult<Query6View> GetInfoAboutTasksByUserId(int id)
        {
            try
            {
                var result = _mapper.Map<Query6View>(_userService.GetInfoAboutTasksByUserId(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
