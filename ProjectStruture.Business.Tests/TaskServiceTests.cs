﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.Business.Services;
using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Repositories;
using ProjectStructure.WebAPI.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using FakeItEasy;
using ProjectStructure.DataAccess.Entities;

namespace ProjectStruture.Business.Tests
{
    public class TaskServiceTests : IDisposable
    {
        private readonly ITaskService _taskService;
        private readonly ITaskRepository _taskRepository;
        private List<Task> tasks = new List<Task>();
        private List<User> users = new List<User>();
        public TaskServiceTests()
        {

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            var _unitOfWork = A.Fake<IUnitOfWork>();
            _taskRepository = A.Fake<ITaskRepository>();
            var _userRepository = A.Fake<IUserRepository>();

            A.CallTo(() => _unitOfWork.Set<Task>()).Returns(_taskRepository);
            A.CallTo(() => _unitOfWork.Set<User>()).Returns(_userRepository);

            A.CallTo(() => _taskRepository.Get()).Returns(tasks);
            A.CallTo(() => _userRepository.Get()).Returns(users);

            _taskService = new TaskService(_unitOfWork, mapper);
        }

        public void Dispose()
        {
            tasks.Clear();
            users.Clear();
        }

        [Fact]
        public void Query2_WhenNoData_ThenThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => _taskService.GetTasksByUserIdWithShortName(1));
        }

        [Fact]
        public void Query2_WhenCorrectTask_ThenSuccess()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { Id = 1, PerformerId = 1, Name = "-" });

            var query2 = _taskService.GetTasksByUserIdWithShortName(users[0].Id);

            Assert.Single(query2);
            Assert.Equal(tasks[0].Id, query2[0].Id);
        }

        [Fact]
        public void UpdateTaskState_WhenTaskDoNotExists_ThenArgumentNullException()
        {
            TaskDTO taskDTO = new TaskDTO { Id = 1, State = 2, FinishedAt = DateTime.Now };

            Assert.Throws<ArgumentNullException>(() => _taskService.Update(taskDTO));
        }

        [Fact]
        public void UpdateTaskState_WhenCorrectData_ThenSuccess()
        {
            tasks.Add(new Task { Id = 1, State = 1 });

            TaskDTO taskDTO = new TaskDTO { Id = 1, State = 2, FinishedAt = DateTime.Now };

            _taskService.Update(taskDTO);

            var calls = Fake.GetCalls(_taskRepository).ToList();

            Assert.Equal(3, calls.Count);
            Assert.Equal("Get", calls[0].Method.Name);
            Assert.Equal("Get", calls[1].Method.Name);
            Assert.Equal("Update", calls[2].Method.Name);
        }

        [Fact]
        public void UpdateTaskStateAndName_WhenNameIsAlredyExists_ThenArgumentException()
        {
            tasks.Add(new Task { Id = 1, State = 1 });
            tasks.Add(new Task { Id = 2, State = 1, Name = "Task" });

            TaskDTO taskDTO = new TaskDTO { Id = 1, State = 2, FinishedAt = DateTime.Now, Name = "Task" };

            Assert.Throws<ArgumentException>(() => _taskService.Update(taskDTO));
        }

        [Fact]
        public void Query2_When1TaskWithoutName_ThenThrowArgumentNullException()
        {
            tasks.Add(new Task { Id = 1, PerformerId = 1 });

            Assert.Throws<ArgumentNullException>(() => _taskService.GetTasksByUserIdWithShortName(1));
        }

        [Fact]
        public void Query2_When1TaskWithNameLongerThan44_ThenThrowArgumentNullException()
        {
            tasks.Add(new Task { Id = 1, PerformerId = 1, Name = new string('-', 45) });

            Assert.Throws<ArgumentNullException>(() => _taskService.GetTasksByUserIdWithShortName(1));
        }

        [Fact]
        public void Query3_WhenNoData_ThenThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => _taskService.GetFinishedTaskByUserIdInThisYear(1));
        }

        [Fact]
        public void Query3_When1TaskFinished_ThenSuccess()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { Id = 1, PerformerId = 1, FinishedAt = DateTime.Now });

            var query3 = _taskService.GetFinishedTaskByUserIdInThisYear(users[0].Id);

            Assert.Single(query3);
            Assert.Equal(tasks[0].Id, query3[0].Id);
        }

        [Fact]
        public void Query8_WhenNoData_ThenThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => _taskService.GetAllUncompletedTasksByUserId(1));
        }

        [Fact]
        public void Query8_ThereAre2Uncompletedtasks_ThenThrowArgumentNullException()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { PerformerId = 1, State = 0 });
            tasks.Add(new Task { PerformerId = 1, State = 1 });
            tasks.Add(new Task { PerformerId = 1, State = 2 });
            tasks.Add(new Task { PerformerId = 1, State = 3 });

            var query8 = _taskService.GetAllUncompletedTasksByUserId(users[0].Id);

            Assert.Equal(2, query8.Count);
        }

        [Fact]
        public void Query8_ThereAreNoUncompletedtasks_ThenThrowArgumentNullException()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { PerformerId = 1, State = 2 });
            tasks.Add(new Task { PerformerId = 1, State = 3 });

            Assert.Throws<ArgumentNullException>(() => _taskService.GetAllUncompletedTasksByUserId(users[0].Id));
        }
    }
}
