﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Entities
{
    public class Task : BaseEntity
    {
        [Required]
        public int ProjectId { get; set; }
        public int? PerformerId { get; set; }
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        [StringLength(150, MinimumLength = 3)]
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public User Performer { get; set; }
    }
}
