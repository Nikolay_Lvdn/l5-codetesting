﻿using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
    }
}
