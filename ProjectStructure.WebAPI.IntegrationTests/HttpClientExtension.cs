﻿using Newtonsoft.Json;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public static class HttpClientExtension
    {
        public static async Task<HttpResponseMessage> PostProjectAsync(this HttpClient client, string name = null, int authorId = 0)
        {
            var project = new ProjectView() { Name = name, AuthorId = authorId };
            string jsonProject = JsonConvert.SerializeObject(project);
            return await client.PostAsync("api/Project", new StringContent(jsonProject, Encoding.UTF8, "application/json"));
        }
        public static async Task<HttpResponseMessage> PostTaskAsync(this HttpClient client, string name = null, int performerId = 0, int projectId = 0, int state = 0)
        {
            var task = new TaskView() { Name = name, PerformerId = performerId, ProjectId = projectId, State = state };
            string jsonTask = JsonConvert.SerializeObject(task);
            return await client.PostAsync("api/Task", new StringContent(jsonTask, Encoding.UTF8, "application/json"));
        }
        public static async Task<HttpResponseMessage> PostTeamAsync(this HttpClient client, string name = null)
        {
            var team = new TeamView() { Name = name };
            string jsonTeam = JsonConvert.SerializeObject(team);
            return await client.PostAsync("api/Team", new StringContent(jsonTeam, Encoding.UTF8, "application/json"));
        }
        public static async Task<HttpResponseMessage> PostUserAsync(this HttpClient client, string name = null)
        {
            var user = new UserView() { FirstName = name };
            string jsonUser = JsonConvert.SerializeObject(user);
            return await client.PostAsync("api/User", new StringContent(jsonUser, Encoding.UTF8, "application/json"));
        }

    }
}
