﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.DTO
{
    public class Query4DTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<UserDTO> Members { get; set; }
    }
}
