﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.DTO
{
    public class Query6DTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastUserProject { get; set; }
        public int? LastProjectTasksCount { get; set; }
        public int? CountOfIncompleteOrCanceledTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
