﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.DTO
{
    public class Query1DTO
    {
        public ProjectDTO Project { get; set; }
        public int Count { get; set; }
    }
}
