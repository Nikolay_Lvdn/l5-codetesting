﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.Business.Interfaces
{
    public interface ITaskService
    {
        void Create(TaskDTO taskDTO);
        IList<TaskDTO> GetAll();
        void Update(TaskDTO taskDTO);
        void Delete(int id);
        IList<TaskDTO> GetTasksByUserIdWithShortName(int id);
        IList<Query3DTO> GetFinishedTaskByUserIdInThisYear(int id);
        IList<TaskDTO> GetAllUncompletedTasksByUserId(int id);
    }
}
