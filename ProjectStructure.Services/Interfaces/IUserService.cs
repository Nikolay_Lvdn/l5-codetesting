﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface IUserService
    {
        void Create(UserDTO userDTO);
        IList<UserDTO> GetAll();
        void Update(UserDTO userDTO);
        void Delete(int id);
        IList<Query5DTO> GetOrderedUsersAndTasks();
        Query6DTO GetInfoAboutTasksByUserId(int id);
    }
}
