﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.Business.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                throw new Exception("Wrong request body");

            if (_unitOfWork.Set<Project>().Get().Select(project => project.Name).Contains(projectDTO.Name))
                throw new ArgumentException("Such project is already exists");

            var projectEntity = _mapper.Map<Project>(projectDTO);

            projectEntity.CreatedAt = DateTime.Now;

            _unitOfWork.Set<Project>().Create(projectEntity);
            _unitOfWork.SaveChanges();
        }

        public IList<ProjectDTO> GetAll()
        {
            return _mapper.Map<IList<Project>, IList<ProjectDTO>>(_unitOfWork.Set<Project>().Get());
        }

        public void Update(ProjectDTO projectDTO)
        {
            var projectToUpdate = _unitOfWork.Set<Project>().Get().FirstOrDefault(project => project.Id == projectDTO.Id);

            if (projectToUpdate == null)
                throw new Exception("No such project");

            if (_unitOfWork.Set<Project>().Get()
                .Where(project => project.Id != projectToUpdate.Id)
                .Select(project => project.Name)
                .Contains(projectDTO.Name))
                throw new ArgumentException("Such project is already exists");

            projectToUpdate.AuthorId = projectDTO.AuthorId;
            projectToUpdate.TeamId = projectDTO.TeamId;
            projectToUpdate.Name = projectDTO.Name;
            projectToUpdate.Description = projectDTO.Description;
            projectToUpdate.Deadline = projectDTO.Deadline;
            _unitOfWork.Set<Project>().Update(projectToUpdate);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            var projectToDelete = _unitOfWork.Set<Project>().Get().FirstOrDefault(project => project.Id == id);

            if (projectToDelete == null)
                throw new Exception("No such project");

            _unitOfWork.Set<Project>().Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IList<Query1DTO> GetCountOfTasksOfProjectsByUserId(int id)
        {
            if (!_unitOfWork.Set<User>().Get().Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var projects = _unitOfWork.Set<Project>().Get();
            var tasks = _unitOfWork.Set<Task>().Get();

            var result = projects.Where(project => project.AuthorId == id)
                .ToDictionary(project => project, project => tasks.Count(task => task.ProjectId == project.Id));

            var resultQuery = result.Select(query1DTO => new Query1DTO
            {
                Project = _mapper.Map<ProjectDTO>(query1DTO.Key),
                Count = query1DTO.Value
            }).ToList();

            if (!resultQuery.Any())
                throw new ArgumentNullException("User not author of any project");

            return resultQuery;
        }

        public IList<Query7DTO> GetInfoAboutProjects()
        {
            var tasks = _unitOfWork.Set<Task>().Get();
            var projects = _unitOfWork.Set<Project>().Get();
            var users = _unitOfWork.Set<User>().Get();

            var result = projects.Where(project => project.Description?.Length > 20 || tasks.Count(task=>task.ProjectId==project.Id) < 3)
                .GroupJoin(users,
                project => project.TeamId,
                user => user.TeamId,
                (project, users) => (Project: project, Users: users))
                .Select(data => (
                Project: data.Project,
                LongestTask: data.Project.Tasks?.OrderBy(t => t.Description).LastOrDefault(),
                ShortestTask: data.Project.Tasks?.OrderBy(t => t.Name).FirstOrDefault(),
                CountOfMembers: data.Users?.Count()
                ));

            if (!result.Any())
                throw new ArgumentNullException("There are no projects");

            var resultQuery = result.Select(query7DTO => new Query7DTO
            {
                Project = _mapper.Map<ProjectDTO>(query7DTO.Project),
                LongestTask = _mapper.Map<TaskDTO>(query7DTO.LongestTask),
                ShortestTask = _mapper.Map<TaskDTO>(query7DTO.ShortestTask),
                CountOfMembers = query7DTO.CountOfMembers
            }).ToList();

            return resultQuery;
        }
    }
}
