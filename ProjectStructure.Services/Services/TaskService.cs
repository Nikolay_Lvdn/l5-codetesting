﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.Business.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(TaskDTO taskDTO)
        {
            if (taskDTO == null)
                throw new Exception("Wrong request body");

            if (_unitOfWork.Set<Task>().Get().Select(task => task.Name).Contains(taskDTO.Name))
                throw new ArgumentException("Such task is already exists");

            var taskEntity = _mapper.Map<Task>(taskDTO);

            taskEntity.CreatedAt = DateTime.Now;

            _unitOfWork.Set<Task>().Create(taskEntity);
            _unitOfWork.SaveChanges();
        }

        public IList<TaskDTO> GetAll()
        {
            return _mapper.Map<IList<Task>, IList<TaskDTO>>(_unitOfWork.Set<Task>().Get());
        }

        public void Update(TaskDTO taskDTO)
        {
            var taskToUpdate = _unitOfWork.Set<Task>().Get().FirstOrDefault(task => task.Id == taskDTO.Id);

            if (taskToUpdate == null)
                throw new ArgumentNullException("No such Task");

            if (_unitOfWork.Set<Task>().Get()
                .Where(task => task.Id != taskToUpdate.Id)
                .Select(task => task.Name)
                .Contains(taskDTO.Name))
                throw new ArgumentException("Such task is already exists");

            taskToUpdate.ProjectId = taskDTO.ProjectId;
            taskToUpdate.PerformerId = taskDTO.PerformerId;
            taskToUpdate.Name = taskDTO.Name;
            taskToUpdate.Description = taskDTO.Description;
            taskToUpdate.State = taskDTO.State;
            taskToUpdate.FinishedAt = taskDTO.FinishedAt;
            _unitOfWork.Set<Task>().Update(taskToUpdate);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            var taskToDelete = _unitOfWork.Set<Task>().Get().FirstOrDefault(task => task.Id == id);

            if (taskToDelete == null)
                throw new Exception("No such Task");

            _unitOfWork.Set<Task>().Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IList<TaskDTO> GetTasksByUserIdWithShortName(int id)
        {
            if (!_unitOfWork.Set<User>().Get().Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");
            var tasks = _unitOfWork.Set<Task>().Get();

            List<Task> result = tasks.Where(task => task.PerformerId == id && task.Name?.Length < 45).ToList();

            if (!result.Any())
                throw new ArgumentNullException("There are no such tasks");

            return _mapper.Map<IList<Task>, IList<TaskDTO>>(result);
        }

        public IList<Query3DTO> GetFinishedTaskByUserIdInThisYear(int id)
        {
            if (!_unitOfWork.Set<User>().Get().Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var tasks = _unitOfWork.Set<Task>().Get();

            if (!_unitOfWork.Set<User>().Get().Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            List<(int, string)> result = tasks.Where(task => task.PerformerId == id
                 && task.FinishedAt != null
                 && task.FinishedAt.Value.Year == DateTime.Now.Year)
                .Select(t => (Id: t.Id, Name: t.Name))
                .ToList();

            if (!result.Any())
                throw new ArgumentNullException("There are no such tasks");

            var resultQuery = result.Select(query3DTO => new Query3DTO
            {
                Id = query3DTO.Item1,
                Name = query3DTO.Item2
            }).ToList();

            return resultQuery;
        }

        public IList<TaskDTO> GetAllUncompletedTasksByUserId(int id)
        {
            if (!_unitOfWork.Set<User>().Get().Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var tasks = _unitOfWork.Set<Task>().Get();

            List<Task> result = tasks.Where(task => task.PerformerId == id && task.State != 2 && task.State != 3).ToList();

            if (!result.Any())
                throw new ArgumentNullException("There are no such tasks");

            return _mapper.Map<IList<Task>, IList<TaskDTO>>(result);
        }

    }
}
